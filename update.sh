#!/usr/bin/env sh

VIM_DIR='lilypond/vim/'

install_part () { local KIND=$1
  mkdir -p $KIND
  cp -fv $VIM_DIR/lilypond-$KIND.vim $KIND/lilypond.vim
}

git clone https://github.com/lilypond/lilypond.git
install_part compiler
install_part ftdetect
install_part ftplugin
install_part indent

install_part syntax
pushd lilypond/
python scripts/build/lilypond-words.py --words --vim --dir=../syntax/
python scripts/build/lilypond-words.py --words --dir=..
popd

cp lilypond/LICENSE .

VER=$(grep VERSION_STABLE lilypond/VERSION | cut -f2 -d=)
DEV=$(grep VERSION_DEVEL lilypond/VERSION | cut -f2 -d=)
HASH=$(git -C lilypond/ rev-parse --short HEAD)
echo "Lilypond Version: ${VER} (${DEV}, git:$HASH)"

rm -rf lilypond
